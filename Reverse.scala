package com.nephila.reverse

/**
  * Scala Application to reverse input arguments and return as a single string
  */
object Reverse extends App{
  val reversedArgs = Reverser.reverse(args)
  println(s"Application output = $reversedArgs")
  if (reversedArgs == "World Hello") {
    sys.exit(0)
  } else {
    sys.exit(1)
  }
}

object Reverser {
  /**
    * Reverse an array of strings and return as a single string
    * @param in Array of strings
    * @return   space separated string of input
    */
  def reverse(in: Array[String]) = in.reverse.mkString(" ")
}


